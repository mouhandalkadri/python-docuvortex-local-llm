from args import parse_command_args
from dotenv import load_dotenv
from ingest.vortex_ingester import VortexIngester

load_dotenv()


def main():
    args = parse_command_args(True)
    ingester = VortexIngester(args.docs, args.instruct, args.store)
    ingester.ingest()


if __name__ == "__main__":
    main()
