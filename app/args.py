import argparse

DEFAULT_MODEL_NAME = "TheBloke/wizardLM-7B-HF"
DEFAULT_INSTRUCT_NAME = "hkunlp/instructor-xl"


def parse_command_args(includeDocs: bool):
    parser = argparse.ArgumentParser(prog='LLM')
    parser.add_argument("-m", "--model", default=DEFAULT_MODEL_NAME)
    parser.add_argument("-i", "--instruct", default=DEFAULT_INSTRUCT_NAME)
    parser.add_argument("-s", "--store", required=True)
    if includeDocs:
        parser.add_argument("-d", "--docs", required=True)
    return parser.parse_args()
